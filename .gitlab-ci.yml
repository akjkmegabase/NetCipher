stages:
  - test
  - deploy

.base:
  image: registry.gitlab.com/fdroid/ci-images-client:latest
  before_script:
    - export GRADLE_USER_HOME=$PWD/.gradle
    - export ANDROID_COMPILE_SDK=`sed -n 's,.*compileSdkVersion\s*\([0-9][0-9]*\).*,\1,p' libnetcipher/build.gradle`
    - alias sdkmanager="sdkmanager --no_https"
    - echo y | sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" > /dev/null
    # limit RAM usage for all gradle runs
    - export maxmem=$(expr $(sed -n 's,^MemAvailable:[^0-9]*\([0-9][0-9]*\)[^0-9]*$,\1,p' /proc/meminfo) / 1024 / 2 / 1024 \* 1024)
    - printf "\norg.gradle.jvmargs=-Xmx${maxmem}m -XX:MaxPermSize=${maxmem}m\norg.gradle.daemon=false\norg.gradle.parallel=false\n" >> gradle.properties
  after_script:
    # this file changes every time but should not be cached
    - rm -f $GRADLE_USER_HOME/caches/modules-2/modules-2.lock
    - rm -fr $GRADLE_USER_HOME/caches/*/plugin-resolution/
  cache:
    paths:
      - .gradle/wrapper
      - .gradle/caches

.test-template: &test-template
  extends: .base
  stage: test
  artifacts:
    name: "${CI_PROJECT_PATH}_${CI_JOB_STAGE}_${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHA}"
    paths:
      - kernel.log
      - logcat.txt
      - "tor.log"
      - "syslog"
      - "*/build/reports"
      - "*/build/outputs/*ml"
      - "*/build/outputs/apk"
    expire_in: 1 week
    when: on_failure
  after_script:
    - echo "Download debug artifacts from https://gitlab.com/${CI_PROJECT_PATH}/-/jobs"

# Run the most important first. Then we can decide whether to ignore
# the style tests if the rest of the more meaningful tests pass.
test:
  <<: *test-template
  script:
    # print lint errors/warnings to build log
    - sed -i 's,\s*textReport .*,\ttextReport true,g' */build.gradle
    # 'assemble' everything and run all checks that do not require a device/emulator
    - ./gradlew build -PdisablePreDex
    # test release process
    - ./gradlew jarRelease -PdisablePreDex

errorprone:
  extends: .base
  stage: test
  script:
    - for f in */build.gradle; do cat tools/errorprone.gradle >> $f; done
    - ./gradlew assembleDebug -PdisablePreDex

# Run the tests in the emulator.  Each step is broken out to run on
# its own since the CI runner can have limited RAM, and the emulator
# can take a while to start.
.connected-template: &connected-template
  retry:
    when:
      - api_failure
      - runner_system_failure
  script:
    - ./gradlew assembleDebug
    - export AVD_SDK=`echo $CI_JOB_NAME | awk '{print $2}'`
    - export AVD_TAG=`echo $CI_JOB_NAME | awk '{print $3}'`
    - export AVD_ARCH=`echo $CI_JOB_NAME | awk '{print $4}'`
    - export AVD_PACKAGE="system-images;android-${AVD_SDK};${AVD_TAG};${AVD_ARCH}"
    - echo $AVD_PACKAGE

    - alias sdkmanager
    - ls -l ~/.android

    - if [ $AVD_SDK -ge 24 ]; then
          set -x;
          apt-get update;
          apt-get -qy install jq netcat-openbsd tor --no-install-recommends;
          service tor start;
          while ! nc -w 1 localhost 9050; do echo "waiting for proxy port..."; sleep 1; done;
          while ! curl --proxy socks5://localhost:9050 --location https://check.torproject.org/api/ip | jq --exit-status '.IsTor'; do echo 'waiting for tor...'; sleep 1; done;
          set +x;
      fi

    - adb start-server
    - start-emulator
    - wait-for-emulator
    - adb devices
    - adb shell input keyevent 82 &
    # these tests only work in Android Studio, not from command line :-|
    - export EXCLUDES="--exclude-task :netcipher-conscrypt:connectedDebugAndroidTest --exclude-task :netcipher-webkit:connectedDebugAndroidTest"
    - ./gradlew connectedCheck $EXCLUDES
      || ./gradlew connectedCheck $EXCLUDES
      || ./gradlew connectedCheck $EXCLUDES
      || (adb -e logcat -d > logcat.txt; exit 1)

no-accel 23 default x86_64:
  <<: *test-template
  <<: *connected-template

no-accel 24 default x86_64:
  <<: *test-template
  <<: *connected-template

.kvm-template: &kvm-template
  tags:
    - fdroid
    - kvm
  only:
    variables:
      - $RUN_KVM_JOBS
  <<: *test-template
  <<: *connected-template

kvm 18 default x86:
  <<: *kvm-template

kvm 21 default x86:
  <<: *kvm-template

kvm 22 default x86:
  <<: *kvm-template

kvm 25 default x86:
  <<: *kvm-template

kvm 29 microg x86_64:
  <<: *kvm-template

jarRelease:
  extends: .base
  stage: deploy
  artifacts:
    name: "${CI_PROJECT_PATH}_${CI_JOB_STAGE}_${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHA}"
    paths:
      - "logcat.txt"
      - "*/build/libs"
      - "*/build/reports"
      - "*/build/outputs/*ml"
      - "*/build/outputs/apk"
    expire_in: 1 week
    when: always
  script:
    - ./make-release-build

pages:
  extends: .base
  stage: deploy
  artifacts:
    paths:
    - public
  script:
    - ./gradlew javadoc
    - test -d public || mkdir public
    - printf '<!DOCTYPE html>\n\n<html><body><h1>NetCipher Javadoc</h1><ul>' > public/index.html
    - for f in */build/docs/javadoc; do
        libname=`echo $f | cut -d / -f 1`;
        printf "<li><a href=\"$libname\/\">$libname</a></li>" >> public/index.html;
        cp -a $f public/$libname;
      done
    - printf '</ul></body></html>' >> public/index.html

after_script:
    # this file changes every time but should not be cached
    - rm -f $GRADLE_USER_HOME/caches/modules-2/modules-2.lock
    - rm -fr $GRADLE_USER_HOME/caches/*/plugin-resolution/
    - (cp /var/log/tor/log tor.log; chmod a+r tor.log) || true
    - (cp /var/log/syslog syslog; chmod a+r syslog) || true
